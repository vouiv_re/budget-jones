<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Budget;

class BudgetFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i=0; $i < 6 ; $i++) { 
            $budget = new Budget();
            $budget->setName("Budget_{$i}");
            $budget->setSum($i*10-5);
            $manager->persist($budget);
        }

        $manager->flush();
    }
}
