import { Budget } from "./budget";

export interface User {
  id:number;
  username:string;
  password:string;
  budgets:Array<Budget>;
}
