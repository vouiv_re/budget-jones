import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { OperationService } from '../service/operation.service';
import { Operation } from '../entity/operation';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {



  @Input() updated: Operation;
  @Output() submit = new EventEmitter<any>();

  constructor(private service: OperationService) { }

  ngOnInit() {
  }

  onClick(updated) {
    this.submit.emit(updated);
  }
}
