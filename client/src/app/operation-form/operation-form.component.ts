import { Component, OnInit } from '@angular/core';
import { Operation } from '../entity/operation';
import { OperationService } from '../service/operation.service';
import { Budget } from '../entity/budget';

@Component({
  selector: 'app-operation-form',
  templateUrl: './operation-form.component.html',
  styleUrls: ['./operation-form.component.css']
})
export class OperationFormComponent implements OnInit {

  newOperation: Operation = { sum: null, description: null, date: null, budgets: [], tags: [],type:null };
  operations: Operation[] = [];
  total: number;
  input: number;
  output: number;
  selected: Operation = { id: 0, sum: 0, description: null, date: null, tags: [], budgets: [],type:null };
  tag : string;

  constructor(private service: OperationService) {
  }

  ngOnInit() {
    this.service.findAll().subscribe(response => this.operations = response.reverse());
    this.service.getTotal().subscribe(response => this.total = response.total);
    this.service.getTotalInput().subscribe(response => this.input = response.totalInput);
    this.service.getTotalOutput().subscribe(response => this.output = response.totalOutput);

  }

  addOperation() {

    this.service.add(this.newOperation).subscribe((response) => {
      this.operations.push(response);
      this.newOperation.tags.push(this.tag);
      this.newOperation.description = null;
      this.newOperation.sum = null;
      this.ngOnInit();
    })
  }

  deleteOperation(newOperation: Operation) {
    this.service.delete(newOperation.id).subscribe((response) => {
      this.operations = this.operations.filter(currentOperation => currentOperation.id !== newOperation.id);
      this.ngOnInit();
    })
  }

  updateOperation(newOperation: Operation) {
    this.service.update(newOperation).subscribe(() => {
      this.selected = { id: 0, sum: 0, description: null, date: null, tags: [], budgets: [],type:null };
      this.ngOnInit();
    })
  }
}

