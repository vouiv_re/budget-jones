import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { FormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { OperationFormComponent } from './operation-form/operation-form.component';
import { UpdateFormComponent } from './update-form/update-form.component';
library.add(fas); // Font-Awesome

const appRoutes: Routes = [
  { path: "", component: OperationFormComponent},
  { path: "update-form", component: UpdateFormComponent},
  { path: "budgets", component: BudgetComponent},
  { path: "budgets/:id", component: BudgetCardComponent},
]
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { BudgetComponent } from './budget/budget.component';
import { BudgetCardComponent } from './budget-card/budget-card.component';
library.add(fas);

@NgModule({
  declarations: [
    AppComponent,
    OperationFormComponent,
    UpdateFormComponent,
    NavbarComponent,
    FooterComponent,
    BudgetComponent,
    BudgetCardComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes, {enableTracing: true}),
    FontAwesomeModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
